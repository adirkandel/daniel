var header = $("header");
$(window).scroll($.throttle(180, function (e) {
	if ($(window).scrollTop() > 200) header.addClass("dark-header");
	else header.removeClass("dark-header");
}));


$(".main-nav .nav-link").click(function (e) {
	e.preventDefault();
	var sectionID = $(this).attr("href");
	console.log(sectionID);
	$("body, html").animate({scrollTop: $(sectionID).offset().top}, 1000);
});


/*
var lightbox = $(".portfolio .previews a").simpleLightbox();

$(".portfolio .previews").owlCarousel({
	items: 1,
	loop: true,
	rtl: true,
	nav: true
});

$(".previews img").click(function () {
	lightbox.open();
});*/
