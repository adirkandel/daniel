<!doctype html>
<html lang="he" dir="rtl">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	
	<title>דניאל שאש - אדריכלות ועיצוב פנים</title>
	
<!--	<link rel="stylesheet" href="/css/bootstrap-4.min.css">-->
	<link rel="stylesheet" href="/css/bootstrap-4-rtl.min.css">
	<link rel="stylesheet" href="/css/style.css">
</head>
<body>
<?php include_once "assets/icons.svg" ?>
<header>
	<nav class="navbar navbar-expand flex-row-reverse">
		<a class="navbar-brand d-flex flex-row-reverse align-items-center mr-0" href="http://www.daniel-shash.co.il">
			<svg><use xlink:href="#logo_icon"></use></svg>
			<div class="mr-3">
				<h2 class="mb-0">DANIEL SHASH</h2>
				<h3 class="mb-0">Architecture & interior design</h3>
			</div>
		</a>
		
		<ul class="social-networks navbar-nav">
			<li class="nav-item">
				<a href="" class="nav-link">
					<svg><use xlink:href="#facebook_icon"></use></svg>
					<span class="sr-only">Facebook</span>
				</a>
			</li>
			<li class="nav-item">
				<a href="" class="nav-link">
					<svg><use xlink:href="#instagram_icon"></use></svg>
					<span class="sr-only">Instagram</span>
				</a>
			</li>
		</ul>
		
		<?php
		/*<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>*/
		?>
		<div class="main-nav collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#about">אודות</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#portfolio">פרויקטים נבחרים</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">צור קשר</a>
				</li>
			</ul>
		</div>
	</nav>
</header>

<div class="parallax-img">
	<img src="/assets/main_back.jpg" alt="interior design">
</div>

<main>
	<section id="about" class="about">
		<div class="wrapper col-lg-11 m-auto">
			<div class="avatar">
				<img src="/assets/avatar.jpeg" alt="דניאל שאש - אדריכלית ומעצבת פנים">
			</div>
			
			<h2 class="h1">קצת על עצמי...</h2>
			
			<p>
				והוא רקטות צרפתית קרן על, מה רבה מיזמי מיוחדים וספציפיים. אל לוח למנוע רוסית לרפובליקה דת שער ספרות בקלות. בדף ויקי תיאטרון בה,<br>
				או ארץ קהילה בהבנה. בהשחתה פוליטיקה גם בקר, בה לוח יוני ננקטת תאולוגיה, פוליטיקה ביולוגיה ב שתי. אחד דת ויקי אחרים, המלצת החופשית אל עזה.<br><br>
				
				סדר את המשפט העברית, בחירות טכניים את בדף. אם רוסית והנדסה לוח. את הבקשה המלחמה התפתחות כתב,<br>
				ניווט גרמנית ב רבה, צעד אספרנטו ואלקטרוניקה אל. המחשב נוסחאות זאת ב. ארץ שמות עמוד על, בשפה בהבנה אם אנא.<br><br>
				
				את בשפה תקשורת מונחונים חפש. אל הטבע היום תרומה צ'ט, ויש או החול ראשי הסביבה, זכר כיצד לציין בויקיפדיה דת. תנך בקרבת יוצרים וספציפיים אל,<br>
				לחבר לעריכת של זאת, אם אינטרנט מיוחדים תנך. מה ולחבר המלחמה קישורים כלל, ארץ צילום החברה מונחונים מה, של המחשב כלליים חפש. ה<br>
				שפט התפתחות על מלא, גם אחר לחבר הבאים, מתוך העיר ויש ב. ב רביעי לעריכה גיאוגרפיה עזה, תנך ב לכאן שאלות שדרו<br><br>
				
				היא העיר חבריכם של, מפתח העזרה למאמרים בקר מה. שתי מיזמי בדפים בהיסטוריה של. זאת מיותר המשפט על, מה לחבר משפטית בהתייחסות שתי.<br>
				עזרה חבריכם של שכל, כדי מה ברית בלשנות הגולשות, ביולי אקראי וכמקובל אם ארץ. של המלצת הבהרה מדע. מדעי לויקיפדים טכנולוגיה ב זכר, מונחונים מיתולוגיה<br>
				אל ארץ, של כדי חשמל הגולשות אנתרופולוגיה.
			</p>
		</div>
	</section>
	
	<section id="portfolio" class="portfolio">
		<div class="wrapper">
			<h2 class="h1">פרויקטים נבחרים</h2>
			
			<ul class="projects list-unstyled">
				<li class="item">
					<div class="back_img">
						<img src="/assets/portfolio/wide_1.jpg">
					</div>
					
					<div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 d-flex flex-row">
						<div class="row">
							<div class="col-12 col-sm-6">
								<h3>פרויקט מס' 1</h3>
								<p>
									והוא רקטות צרפתית קרן על, מה רבה מיזמי מיוחדים וספציפיים. אל לוח למנוע רוסית לרפובליקה,<br>
									דת שער ספרות בקלות. בדף ויקי תיאטרון בה, או ארץ קהילה בהבנה. בהשחתה פוליטיקה גם בקר,<br>
									בה לוח יוני ננקטת תאולוגיה, פוליטיקה ביולוגיה ב שתי. אחד דת ויקי אחרים, המלצת החופשית אל עזה.
								</p>
							</div>
							<div class="col-12 col-sm-6">
								<ul class="gallery row list-unstyled">
									<li class="col-6 col-sm-4">
										<img src="/assets/portfolio/tbn_1.jpg" alt="" class="img-fluid">
									</li>
									<li class="col-6 col-sm-4">
										<img src="/assets/portfolio/tbn_2.jpg" alt="" class="img-fluid">
									</li>
									<li class="col-6 col-sm-4">
										<img src="/assets/portfolio/tbn_3.jpg" alt="" class="img-fluid">
									</li>
									<li class="col-6 col-sm-4">
										<img src="/assets/portfolio/tbn_4.jpg" alt="" class="img-fluid">
									</li>
									<li class="col-6 col-sm-4">
										<img src="/assets/portfolio/tbn_5.jpg" alt="" class="img-fluid">
									</li>
									<li class="col-6 col-sm-4">
										<img src="/assets/portfolio/tbn_6.jpg" alt="" class="img-fluid">
									</li>
								</ul>
							</div>
						</div>
					</div>
				</li>
			</ul>
			<!--<div class="row flex-row-reverse align-items-stretch">
				<div class="col-12 col-md-8">
					<div class="previews owl-carousel">
						<a href="/assets/portfolio/wide_1.jpg"><img class="embed-responsive-item" src="/assets/portfolio/wide_1.jpg"></a>
						<a href="/assets/portfolio/wide_1.jpg"><img class="embed-responsive-item" src="/assets/portfolio/wide_1.jpg"></a>
						<a href="/assets/portfolio/wide_1.jpg"><img class="embed-responsive-item" src="/assets/portfolio/wide_1.jpg"></a>
						<a href="/assets/portfolio/wide_1.jpg"><img class="embed-responsive-item" src="/assets/portfolio/wide_1.jpg"></a>
					</div>
				</div>
				<div class="col-12 col-md-4 d-flex flex-column align-items-stretch">
					<h3>פרויקט מס' 1</h3>
					<p>
						והוא רקטות צרפתית קרן על, מה רבה מיזמי מיוחדים וספציפיים. אל לוח למנוע רוסית לרפובליקה,<br>
						דת שער ספרות בקלות. בדף ויקי תיאטרון בה, או ארץ קהילה בהבנה. בהשחתה פוליטיקה גם בקר,<br>
						בה לוח יוני ננקטת תאולוגיה, פוליטיקה ביולוגיה ב שתי. אחד דת ויקי אחרים, המלצת החופשית אל עזה.
					</p>
					<ul class="projects row list-unstyled">
						<li class="col-6 col-sm-4">
							<img src="/assets/portfolio/tbn_1.jpg" alt="" class="img-fluid">
						</li>
						<li class="col-6 col-sm-4">
							<img src="/assets/portfolio/tbn_2.jpg" alt="" class="img-fluid">
						</li>
						<li class="col-6 col-sm-4">
							<img src="/assets/portfolio/tbn_3.jpg" alt="" class="img-fluid">
						</li>
						<li class="col-6 col-sm-4">
							<img src="/assets/portfolio/tbn_4.jpg" alt="" class="img-fluid">
						</li>
						<li class="col-6 col-sm-4">
							<img src="/assets/portfolio/tbn_5.jpg" alt="" class="img-fluid">
						</li>
						<li class="col-6 col-sm-4">
							<img src="/assets/portfolio/tbn_6.jpg" alt="" class="img-fluid">
						</li>
					</ul>
				</div>
			</div>-->
		</div>
	</section>
	
	<section class="contact_us">
		<div class="container">
			<h2 class="h1">אשמח להיפגש אתכם לכוס קפה</h2>
			<h3 class="h2">שלחו לי הודעה או חפשו אותי ברשתות החברתיות</h3>
			
			<ul class="social-networks navbar-nav d-flex flex-row justify-content-center">
				<li class="nav-item">
					<a href="" class="nav-link d-flex align-items-center justify-content-center">
						<svg class="facebook_icon"><use xlink:href="#facebook_icon"></use></svg>
						<span class="sr-only">Facebook</span>
					</a>
				</li>
				<li class="nav-item">
					<a href="" class="nav-link d-flex align-items-center justify-content-center">
						<svg><use xlink:href="#instagram_icon"></use></svg>
						<span class="sr-only">Instagram</span>
					</a>
				</li>
			</ul>
		</div>
	</section>
</main>

<div class="contact_back">
	<img src="/assets/contact_back.jpg" alt="">
</div>

<footer class="d-flex align-items-center justify-content-between">
	<p class="mb-0">כל הזכויות שמורות לדניאל שאש אדריכלות ועיצוב פנים</p>
	
	<svg><use xlink:href="#logo_icon"></use></svg>
</footer>


<link rel="stylesheet" href="/css/owl.carousel.min.css">
<link rel="stylesheet" href="/css/simplelightbox.min.css">

<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/simple-lightbox.min.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/jquery.ba-throttle-debounce.min.js"></script>
<script src="/js/scripts.js"></script>

</body>
</html>